package common

import (
	"fmt"
	"os"
	"runtime"

	"gitee.com/satyr/tools/conf"
)

//Response ..
type Response struct {
	ErrNo   int64       `json:"err_no"`
	ErrMsg  string      `json:"err_msg"`
	Results interface{} `json:"results"`
}

// IsExist returns a boolean indicating whether a file or directory exist.
func IsExist(filepath string) bool {
	_, err := os.Stat(filepath)
	if err == nil {
		return true
	}
	return !os.IsNotExist(err)
}

func GetTest() {
	fmt.Println(fmt.Sprintf("%+v", conf.Configuration().Server))

}

//用于打印panic时的堆栈
func GetStack() []byte {
	buf := make([]byte, 1<<12) //16kb
	num := runtime.Stack(buf, false)

	return buf[:num]
}
