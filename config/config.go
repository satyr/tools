package config

import (
	"fmt"
	"os"

	"gitee.com/satyr/tools/conf"
	"gitee.com/satyr/tools/logger"

	"github.com/pelletier/go-toml/v2"
)

// LoadConfig 加载配置
func LoadConfig(file string) (err error) {
	logger.Infof("get config from file [%s]", file)
	fr, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
		return
	}

	decoder := toml.NewDecoder(fr)
	decoder.DisallowUnknownFields()
	decoder.Decode(conf.Configuration())
	fr.Close()

	return nil
}
