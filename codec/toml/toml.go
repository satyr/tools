package toml

import (
	tomlLib "github.com/pelletier/go-toml/v2"
)

var (
	Marshal   = tomlLib.Marshal
	Unmarshal = tomlLib.Unmarshal
)
