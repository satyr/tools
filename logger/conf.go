package logger

import (
	"fmt"
	"strings"

	"go.uber.org/zap/zapcore"
)

type Config struct {
	LogFile   string
	LogLevel  string
	IsConsole bool
	LogType   string
	LogMaxNum int32
	LogSize   int64
	LogUnit   string
}

var _Config Config

func Configuration() *Config {
	return &_Config
}

const (
	rollTypeTimeDaily = "daily"
	rollTypeFileSize  = "roll"
)

const (
	DebugLevel  = zapcore.DebugLevel
	InfoLevel   = zapcore.InfoLevel
	WarnLevel   = zapcore.WarnLevel
	ErrorLevel  = zapcore.ErrorLevel
	DPanicLevel = zapcore.DPanicLevel
	PanicLevel  = zapcore.PanicLevel
	FatalLevel  = zapcore.FatalLevel
)

func ToGBSize(unit string, size int64) (int, error) {
	switch strings.ToUpper(unit) {
	case "K", "KB":
		return int(size / 1024), nil
	case "M", "MB":
		return int(size), nil
	case "G", "GB":
		return int(size * 1024), nil
	case "T", "TB":
		return int(size * 1024 * 1024), nil
	default:
		return 0, fmt.Errorf("%s not support unit", unit)
	}
}
