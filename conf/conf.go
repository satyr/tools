package conf

import "gitee.com/satyr/tools/logger"

var _TestConfig *TestConfig

func init() {
	_TestConfig = new(TestConfig)
}

func Configuration() *TestConfig {
	return _TestConfig
}

type TestConfig struct {
	SnakeFlag int
	Server    Server
	Gateway   []string
	Logger    logger.Config
	Db        Db
	Route     Route
	Template  Template
}

type Server struct {
	Address     string
	Concurrence int
}

type Db struct {
	Username string
	Password string
	Protocol string
	Address  string
	Port     string
	Dbname   string
	Params   string
}

type Route struct {
	DefaultAction string
}

// render template
type Template struct {
	StaticPath  string
	HTMLPath    string
	WidgetsPath string
	IsCache     bool
}
