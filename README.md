
# 工具包使用方法

```go
   // 初始化
   Init()
   // 加载配置文件
   config.LoadConfig("conf/test.toml")
   logger.InitLogger(conf.Configuration().Logger)
```


# API请求 - POST方式
> Content-Type", "application/json"
```go

type Respond struct {
   ErrNo   int           `json:"err_no"`
   ErrMsg  string        `json:"err_msg"`
   Results interface{}   `json:"results"`
}


resp :=  Respond
httpCtx := ctx.NewHTTPContext()
address := []string{"http://127.0.0.1"}
uri := "/api/v1/user/get"
body := nil
err := api.Call(httpCtx, header, address, uri, body, &resp)

```