package tools

import (
	"sync"

	"gitee.com/satyr/tools/common"
	"gitee.com/satyr/tools/logger"
	"gitee.com/satyr/tools/signal"
)

var (
	once sync.Once
)

func Init() (err error) {
	once.Do(func() {
		err = common.ParseFlag()
		if err != nil && !common.IsGoTest() {
			logger.Fatal(err)
		}

		go signal.Init()

		//err = utils.GeneratePIDFile(filepath.Join(common.GetAppPath(), "pid"))
		//if err != nil {
		//	logger.Fatal(err)
		//}
	})
	return
}
